import logging


def get_logger(job_id, file_path):
    logger = logging.getLogger(job_id)
    logger.setLevel(logging.INFO)

    # Create handlers
    c_handler = logging.StreamHandler()
    f_handler = logging.FileHandler(file_path, 'w')
    c_handler.setLevel(logging.INFO)
    f_handler.setLevel(logging.INFO)

    # Create formatters and add it to handlers
    fmt = logging.Formatter('%(name)s - %(asctime)s - %(message)s', "%Y-%m-%d %H:%M:%S")
    c_handler.setFormatter(fmt)
    f_handler.setFormatter(fmt)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    logger.addHandler(f_handler)

    return logger
