#### Introduction
Pype is short for python pipeline. It is a framework for programatically generating and executing configuration files for complex pipelines. It includes a python-based CLI for running jobs implementend for the framework.


#### Installation
To install run
```
$ pip install pype-ms
```

or
```
$ git clone https://gitlab.com/matschreiner/pype
$ cd pype
$ pip install .

```

#### Usage

We refer to scripts that are compatible with pype as 'jobs' and in order for a job to be runnable through pype it must

* Be importable by the Python-interpreter
* Contain a main function that takes a config and an optional logger as arguments
* Implement a Configuration-class inhereting from pype.BaseConfig


Two simple examples of jobs written for pype are found in the `example/jobs/`
The `write_greeting` takes a name as a parameter and writes a msg.txt as an output

```
# example/jobs/write_greeting.py
from pype.baseconfig import BaseConfig

class Config(BaseConfig):
    script_path = "example/jobs/write_greeting.py"
    params = {"name": "John Doe"}
    outputs = {"msg": "msg.txt"}


def main(config, logger):
    logger.info("running job")
    name = config['params']['name']
    msg = f"Hello {name}!"
    with open(config['outputs']['msg'], 'w') as f:
        f.write(msg)
```

The `print_greeting.py` job takes a text file as input and prints it to the terminal.
```
# example/jobs/print_greeting.py
from pype.baseconfig import BaseConfig

class Config(BaseConfig):
    script_path = "example/jobs/print_greeting.py"
    inputs = {"msg"}


def main(config, logger):
    logger.info("running job")
    msg = open(config['inputs']['msg'],'r').read()
    print(msg)
```

A simple pipeline would be generating a text file with the `write_greeting.py` job and then printing it with the `print_greeting.py` job.
In order to create a pipeline we need to write a pipeline configuration script.

The following pipeline can be found in the example_pipeline folder:


```
from example.jobs import print_greeting, write_greeting

write_job = write_greeting.Config(
    job_id = 'pipeline/write_job',
    params = {"name": "Mathias Schreiner"}
)

print_job = print_greeting.Config(
    job_id = 'pipeline/print_job',
    inputs = {"msg": write_job['outputs']['msg']}
)
```

To generate the pipeline run
```
$ python example/example_pipeline/pipeline_config.py
```

This would generate the following file structure:
```
example_pipeline/
├─ pipeline_config.py
├─ pipeline_config.yaml
├─ pipeline/
    ├─ write_job/
    │  ├─ config.yaml
    │  ├─ output/
    ├─ print_job/
       ├─ config.yaml
       ├─ output/
```

At this point each job folder contains an empty output folder - the job has not been run yet, and a configuration file for the job.

A single job can be run with
```
$ pype run example/example_pipeline/pipeline/write_job/config.yaml
```

which would result in an output like this to the terminal:
```
pipeline/write_job - 2022-09-09 13:55:01 -
######################################
    Running job pipeline/write_job
######################################

Start: 2022-09-09, 13:55:01
Host: BayesicBrain
Device: PID: 94295
gitsha: 155acbc

Configuration:
{'job_dir': 'example/example_pipeline/pipeline/write_job',
 'job_id': 'pipeline/write_job',
 'output_dir': 'example/example_pipeline/pipeline/write_job/output',
 'outputs': {'msg': 'example/example_pipeline/pipeline/write_job/output/msg.txt'},
 'params': {'name': 'Mathias Schreiner'},
 'qualname': 'Config',
 'script_path': 'example/jobs/write_greeting.py'}

__________________________________________________________________________________

pipeline/write_job - 2022-09-09 13:55:01 - running job
pipeline/write_job - 2022-09-09 13:55:01 - execution took 0:00:00
pipeline/write_job - 2022-09-09 13:55:01 - job terminated succesfully.

-
```

This updates the pipeline directory as follows:

```

example_pipeline
├─ ...
├─ pipeline/
    ├─ ...
    ├─ write_job/
       ├─ output/
       │  ├─ msg.txt
       ├─ Done
       ├─ lock/
       ├─ file.log
       ├─ config.yaml
       ├─ git_sha.txt
```

* __output__ now contains the outputs from the instantiate job which in our case is our message.
* __Done/Running/Failed__  signifies the state of the job - we ran without errors, so status is Done.
* __lock/__ serves as a lock to ensure that, when running jobs in parallel different processes won't start the same job.
* __file.log__ contains logs from the provided logger, as well as logs about job status, error messages, timings, configuration etc.
* __git_sha.txt__ is the current git-sha and can be used to rerun the pipeline with the appropriate version of the repository.


The rest of the pipeline can be run by
```
pype run example/example_pipeline/pipeline_config.yaml
```

In our case the write job has been run already, so it will only run the final job, `print_job` and print the message to the terminal.

In this way complex pipelines can be written such that for example neural network models / datasets / complex training algorithms / evaluation scripts / downstream tasks can be linked together in an easy way.
