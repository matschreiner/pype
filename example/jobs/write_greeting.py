from pype.baseconfig import BaseConfig


class Config(BaseConfig):
    script_path = "example/jobs/write_greeting.py"
    params = {"name": "John Doe"}
    outputs = {"msg": "msg.txt"}


def main(config, logger):
    logger.info("running job")
    name = config['params']['name']
    msg = f"Hello {name}!"
    with open(config['outputs']['msg'], 'w') as f:
        f.write(msg)
