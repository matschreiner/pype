from pype.baseconfig import BaseConfig


class Config(BaseConfig):
    script_path = "example/jobs/print_greeting.py"
    inputs = {"msg"}


def main(config, logger):
    logger.info("running job")
    msg = open(config['inputs']['msg'],'r').read()
    print(msg)
