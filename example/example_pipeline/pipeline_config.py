from example.jobs import print_greeting, write_greeting

write_job = write_greeting.Config(
    job_id = 'pipeline/write_job',
    params = {"name": "Mathias Schreiner"}
)

print_job = print_greeting.Config(
    job_id = 'pipeline/print_job',
    inputs = {"msg": write_job['outputs']['msg']}
)
